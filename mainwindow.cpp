#include "mainwindow.h"
#include <DMainWindow>
#include "mainwindow.h"
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QProcess>
#include <QIcon>
DWIDGET_USE_NAMESPACE

MainWindow::MainWindow(QWidget *parent)
    : DMainWindow(parent)
{
    w = new Widget;

    resize(w->size()); //设置窗口大小
    setFixedSize(494,300);
    setWindowIcon(QIcon(":/images/builder.png"));
    setCentralWidget(w);
}

MainWindow::~MainWindow()
{

}
