#include "mainwindow.h"
#include <DApplication>
#include <DWidgetUtil>  //Dtk::Widget::moveToCenter(&w); 要调用它，就得引用DWidgetUtil

DWIDGET_USE_NAMESPACE
int main(int argc, char *argv[])
{
    DApplication::loadDXcbPlugin();  //让bar处在标题栏中
    DApplication a(argc, argv);

     a.setAttribute(Qt::AA_UseHighDpiPixmaps);
     a.loadTranslator();
     a.setOrganizationName("Deepin Community Store");
     a.setApplicationVersion(DApplication::buildVersion("0.1.3"));
     a.setApplicationAcknowledgementPage("http://www.shenmo.tech:420/");
     a.setApplicationAcknowledgementVisible("幻方空间工作室");
     a.setApplicationHomePage("http://www.shenmo.tech:420/");
     a.setProductIcon(QIcon(":/images/builder.png"));  //设置Logo
     a.setProductName("社区商店Appimage投稿器");
     a.setApplicationName("社区商店Appimage投稿器"); //只有在这儿修改窗口标题才有效

    MainWindow w;
    w.show();

    //让打开时界面显示在正中
    Dtk::Widget::moveToCenter(&w);


    return a.exec();
}
