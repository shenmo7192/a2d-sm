#include "widget.h"
#include "ui_widget.h"
#include "mainwindow.h"
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QProcess>
#include <QIcon>
QString Path;
QString AppimageName;
int Size; // 尝试添加Size变量，然而我不会用Qstring，干脆百度

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_2_clicked(bool checked)
{
    QString filePath = QFileDialog::getOpenFileName(nullptr,("打开文件"),QDir::homePath(),
            ("AppImage(*.AppImage *.appimage);;所有文件(*.*)"));
    QFileInfo fileInfo;
    fileInfo=QFileInfo(filePath);
    AppimageName=fileInfo.fileName();
    Path=fileInfo.absolutePath();
    Size=fileInfo.size();
}

void Widget::on_pushButton_clicked(bool checked)
{
    QFile fileTemp(QDir::homePath()+"/.config.txt");
    fileTemp.remove();
    QString Name = ui->lineEdit_2->text();
    QString Version = ui->lineEdit_3->text();
    QFile file(QDir::homePath()+"/"+".config.txt");
        if(! file.open(QIODevice::Append|QIODevice::Text))  //append追加，不会覆盖之前的文件
        {
            QMessageBox::critical(this,"错误","文件打开失败，信息没有保存！","确定");
            return;
        }

        QTextStream out(&file);//写入
        out << AppimageName<<endl;
        out << Path<<endl;
        out << Name<<endl;
        out << Version<<endl;
        out << Size<<endl;
        out.flush();
        file.close();
        QProcess *makePkg = new QProcess(this);
        makePkg->start("a2d-core");//在/usr/bin/
        makePkg->waitForFinished();
        ui->textBrowser->setText(makePkg->readAllStandardOutput());
}
